//
//  QBDisplayTagImageView.swift
//  QBCustomImageTag
//
//  Created by 乐启榜 on 16/8/30.
//  Copyright © 2016年 乐启榜. All rights reserved.
//

import UIKit

extension UIImageView {
    
    func addTagView(point: CGPoint, text: String, isOverTurn: Bool){
        
        let size: CGSize = text.qb_stringSizeWithFont(UIFont.systemFontOfSize(11))
        
        let imageLabelIcon: UIImage = UIImage(named: "textTag")!
        
        let _tagView = QBTagView(frame: CGRect(x: point.x, y: point.y - imageLabelIcon.size.height / 2 , width: size.width + 8 + 15, height: imageLabelIcon.size.height))
        
        _tagView.isOverTurn = isOverTurn
        
        _tagView.textStr = text
        
        addSubview(_tagView)
    }
}
