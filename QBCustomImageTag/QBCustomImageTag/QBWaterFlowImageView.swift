//
//  QBWaterFlowImageView.swift
//  QBCustomImageTag
//
//  Created by 乐启榜 on 16/8/27.
//  Copyright © 2016年 乐启榜. All rights reserved.
//

import UIKit

class QBWaterFlowImageView: UIImageView {
    
    var isOverTurn: Bool = false{
        didSet{
            
            if isOverTurn{
                
                label.frame = CGRect(x: _rightEdge, y: 0, width: frame.width - _rightEdge - _leftEdge, height: frame.height)
                
            }else{
                
                label.frame = CGRect(x: _leftEdge, y: 0, width: frame.width - _rightEdge - _leftEdge, height: frame.height)
            }
        }
    }
    
    var text: String = "" {
        didSet{
            label.text = text
        }
    }
    
    private let label: UILabel = UILabel()
    
    private let _leftEdge: CGFloat = 10.0
    
    private let _rightEdge: CGFloat = 5.0
    
    init() {
        
        super.init(frame: CGRectZero)
        
        label.textColor = UIColor.whiteColor()
        
        label.textAlignment = .Center
        
        label.font = UIFont.systemFontOfSize(11)
        
        self.addSubview(label)
    }
    
    override var frame: CGRect{
        didSet{
            label.frame = CGRect(x: _leftEdge, y: 0, width: frame.width - _rightEdge - _leftEdge, height: frame.height)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

