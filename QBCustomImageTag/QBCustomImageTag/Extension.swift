//
//  Extension.swift
//  QBCustomImageTag
//
//  Created by 乐启榜 on 16/8/27.
//  Copyright © 2016年 乐启榜. All rights reserved.
//

import UIKit

extension NSTimer {
    
    func pauseTimer(){
        
        if !self.isProxy() {
            
            print("NSTimer isProxy false: pauseTimer")
            
            return
        }
        
        self.fireDate = NSDate.distantFuture()
        
        print("distantFuture\(NSDate.distantFuture())")
    }
    
    func resumenTimer() {
        
        if !self.isProxy() {
            
            print("NSTimer isProxy false: resumenTimer")
            
            return
        }
        self.fireDate = NSDate(timeIntervalSinceNow: 0)
    }
    
    func resumeTimerAfterTimeInterval(interval: NSTimeInterval){
        
        if !self.isProxy(){
            
            print("NSTimer isProxy false: resumeTimerAfterTimeInterval")
            
            return
        }
        self.fireDate = NSDate(timeIntervalSinceNow: interval)
    }
}

extension String{
    
    func qb_stringSize(font: UIFont, maxWidth: CGFloat, maxHeight: CGFloat) -> CGSize{
        
        let arrt: Dictionary<String, UIFont> = [NSFontAttributeName: font]
        
        let maxSize: CGSize = CGSize(width: maxWidth, height: maxHeight)
        
        let options: NSStringDrawingOptions = [.TruncatesLastVisibleLine,.UsesLineFragmentOrigin,.UsesFontLeading]
        
        return self.boundingRectWithSize(maxSize, options: options, attributes: arrt, context: nil).size
    }
    
    
    func qb_stringSizeWithFont(font: UIFont) -> CGSize{
        
        return qb_stringSize(font, maxWidth: 0, maxHeight: 0)
    }
}
