//
//  QBTagView.swift
//  QBCustomImageTag
//
//  Created by 乐启榜 on 16/8/27.
//  Copyright © 2016年 乐启榜. All rights reserved.
//

import UIKit

class QBTagView: UIView {
    
    var blnSize: CGFloat = 7.5
    
    var isOverTurn: Bool = false {
        didSet{
            
            configUserInterfaceWithOverTurn(isOverTurn)
        }
    }
    
    var textStr: String = ""{
        didSet{
//            configUserInterfaceWithTextWidth()
            configUserInterfaceWithOverTurn(isOverTurn)
        }
    }
    
    private var timerAnimation: NSTimer?
    
    private lazy var blnView: UIView = {
        
        let _blnView: UIView = UIView()
        
        _blnView.backgroundColor = UIColor(red: 1, green: 96/255.0, blue: 184/255.0, alpha: 1.0)
        
        _blnView.userInteractionEnabled = false
        
        _blnView.layer.cornerRadius = 7.5 / 2
        
        return _blnView
    }()
    
    
    private var blnShadeView: UIView = {
        
        let _blnShadeView = UIView()
        
        _blnShadeView.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.7)
        
        _blnShadeView.userInteractionEnabled = false
        
        _blnShadeView.layer.cornerRadius = 7.5 / 2
        
        return _blnShadeView
    }()
    
    
    private var imageView: QBWaterFlowImageView = {
        let _imageView = QBWaterFlowImageView()
        
        return _imageView
    }()
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        timerAnimation = NSTimer.scheduledTimerWithTimeInterval(3, target: self, selector: #selector(animationTimerDidFired), userInfo: nil, repeats: true)
        
        setUpUserInterface()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    deinit {
        timerAnimation?.invalidate()
        print(timerAnimation)
    }
    
    private func setUpUserInterface() {
        
        addSubview(blnShadeView)
        
        addSubview(blnView)
        
        addSubview(imageView)
        
        blnShadeView.frame = CGRect(x: 0, y: frame.height / 2 - blnSize / 2, width: blnSize, height: blnSize)
        
        blnView.frame = CGRect(x: 0, y: frame.height / 2 - blnSize / 2, width: blnSize, height: blnSize)
    }
    
    /**
     使用文字来创建翻转状态
     */
    private func configUserInterfaceWithTextWidth() {
        
        let textSize: CGSize = textStr.qb_stringSizeWithFont(UIFont.systemFontOfSize(11))
        
        // 设置文字
        imageView.text = textStr
        
        // 设置imageView的宽度跟随文字的宽度变化
        var image = UIImage(named: "textTag")!
        
        let leftCapWidth: Int = Int(image.size.width / 2)
        
        let topCapHeight: Int = Int(image.size.height / 2)
        
        image = image.stretchableImageWithLeftCapWidth(leftCapWidth, topCapHeight: topCapHeight)
        
        imageView.image = image
        
        imageView.frame = CGRect(x: 8, y: 0, width: textSize.width + 15, height: frame.height)
        
        // 设置tagView宽度跟随字体的宽度变化
        bounds = CGRect(x: 0, y: 0, width: imageView.frame.width + 8, height: frame.height)
        
    }
    
    /**
     使用翻转状态来创建View
     
     - parameter _isOverTurn: 翻转状态
     */
    private func configUserInterfaceWithOverTurn(_isOverTurn: Bool){
        
        var image: UIImage
        var imageViewOriginX: CGFloat = 0.0
        var blnOriginX: CGFloat = 0.0
        
        // 设置文字
        imageView.text = textStr

        if _isOverTurn {
            
            image = UIImage(named: "textTagAnti")!
            
            imageViewOriginX = 0
            
            blnOriginX = frame.width - blnSize
            
        } else {
            
            image = UIImage(named: "textTag")!
            
            imageViewOriginX = 8
            
            blnOriginX = 0
        }
        
        // 设置image图片拉伸
        
        let leftCapWidth: Int = Int(image.size.width / 2)
        
        let topCapHeight: Int = Int(image.size.height / 2)
        
        image = image.stretchableImageWithLeftCapWidth(leftCapWidth, topCapHeight: topCapHeight)
        
        imageView.image = image
        
        imageView.frame = CGRect(x: imageViewOriginX, y: 0, width: frame.width - 8, height: frame.height)
        
        imageView.isOverTurn = _isOverTurn
        
        blnShadeView.frame = CGRect(x: blnOriginX, y: frame.height / 2 - blnSize / 2, width: blnSize, height: blnSize)
        
        blnView.frame = CGRect(x: blnOriginX, y: frame.height / 2 - blnSize / 2, width: blnSize, height: blnSize)
        
    }
    
    
    // bln动画效果
    @objc private func animationTimerDidFired(){
        
        weak var weakSelf = self
        guard let _weakSelf = weakSelf else{return}
        
        UIView.animateWithDuration(1.0, animations: {
            
            _weakSelf.blnView.transform = CGAffineTransformMakeScale(1.3, 1.3)
            
        }) { (finished) in
            
            UIView.animateWithDuration(1, animations: {
                
                _weakSelf.blnView.transform = CGAffineTransformIdentity
                
                }, completion: { (finished) in
                    
                    _weakSelf.blnShadeView.alpha = 1.0
                    
                    UIView.animateWithDuration(1.0, animations: {
                        
                        _weakSelf.blnShadeView.alpha = 0.0
                        _weakSelf.blnShadeView.transform = CGAffineTransformMakeScale(5, 5)
                        
                        }, completion: { (finished) in
                            
                            _weakSelf.blnShadeView.transform = CGAffineTransformIdentity
                            
                    })
            })
        }
        
    }
}

