//
//  QBEditorViewController.swift
//  QBCustomImageTag
//
//  Created by 乐启榜 on 16/8/23.
//  Copyright © 2016年 乐启榜. All rights reserved.
//

import UIKit

class QBEditorViewController: UIViewController,QBTagEditorImageViewDelegate,QBSearchTVCDelegate {
    
    var image: UIImage?
    
    private var imageView: QBTagEditorImageView?

    override func viewDidLoad() {
        super.viewDidLoad()
        // 1、创建ImageView

            
        let _imageFrame: CGRect = CGRect(x: 0, y: 200, width: self.view.frame.width, height: self.view.frame.width)
        imageView = QBTagEditorImageView(frame: _imageFrame, image: image!)
        imageView?.delegate = self
        imageView?.text = ""
            
        self.view.addSubview(imageView!)
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func didSeletedWithIndex(index: Int) {
        
        let searchTVC = QBSearchTVC(style: .Plain)
        searchTVC.delegate = self
        self.navigationController?.pushViewController(searchTVC, animated: true)
    }
    func editingWithText(text: String) {
        
        let searchTVC = QBSearchTVC(style: .Plain)
        searchTVC.delegate = self
        searchTVC.text = text
        self.navigationController?.pushViewController(searchTVC, animated: true)
    }
    
    func didSelectedTitleString(text: String){
        imageView?.text = text
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
