//
//  QBRootViewController.swift
//  QBCustomImageTag
//
//  Created by 乐启榜 on 16/8/23.
//  Copyright © 2016年 乐启榜. All rights reserved.
//

import UIKit

class QBRootViewController: UIViewController {
    
    var testImageView: UIImageView?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.cyanColor()
        self.navigationItem.title = "图片添加标注"
        
        let rightBTN: UIButton = UIButton(type: .Custom)
        rightBTN.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        rightBTN.setTitle("编辑", forState: .Normal)
        rightBTN.setTitleColor(UIColor.blackColor(), forState: .Normal)
        rightBTN.addTarget(self, action: #selector(rightBTNActive(_:)), forControlEvents: .TouchUpInside)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: rightBTN)
        
        testImageView = UIImageView(image: UIImage(named: "12345.jpg"))
        testImageView?.frame = CGRect(x: 0, y: 100, width: self.view.frame.width, height: self.view.frame.width)
        view.addSubview(testImageView!)
        
        
        
        testImageView?.addTagView(CGPoint(x: 100, y: 39), text: "hahaa", isOverTurn: false)
        testImageView?.addTagView(CGPoint(x: 90, y: 67), text: "图片添加成功", isOverTurn: true)
        
    }
    func rightBTNActive(sender: UIButton){
        
        let editorVC = QBEditorViewController()
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "返回", style: .Done, target: nil, action: nil)
        editorVC.image = testImageView?.image
        self.navigationController?.pushViewController(editorVC, animated: true)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
