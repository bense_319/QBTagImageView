
 **效果图** 
显示时的效果
![输入图片说明](http://git.oschina.net/uploads/images/2016/0831/182256_008dc292_882385.gif "在这里输入图片标题")

编辑是的效果
点击图片弹的弹窗效果
![输入图片说明](http://git.oschina.net/uploads/images/2016/0831/182546_8ce2329b_882385.png "在这里输入图片标题")
支持多个点，编辑、翻转、删除功能
![输入图片说明](http://git.oschina.net/uploads/images/2016/0831/182558_39c6961b_882385.png "在这里输入图片标题")


这个Demo定义了两个类，按照编辑和显示来区分的，如图：
有这么两个文件
![输入图片说明](http://git.oschina.net/uploads/images/2016/0831/181048_36ed20f8_882385.png "在这里输入图片标题")


 **调用接口** 
需要使用编辑功能的，可以这么调用
![输入图片说明](http://git.oschina.net/uploads/images/2016/0831/181430_f6f57aa7_882385.png "在这里输入图片标题")

需要显示的功能，使用类方法调用！我扩展了UIImageView的类，可以这么调用
![输入图片说明](http://git.oschina.net/uploads/images/2016/0831/181644_83405784_882385.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2016/0831/181926_d175beac_882385.png "在这里输入图片标题")